//============================
//
// ゲーム画面のヘッダー
// Author:hamada ryuuga
//
//============================
#ifndef _GAME_H_		//このマクロが定義されてなかったら
#define _GAME_H_		//2重インクルード防止のマクロ定義

#include "object.h"
#include "stage.h"

class CPause; 
class CStage;
class CMagicBox;
class CParticleManager;

class CGame :public CObject
{
public:
	CGame();
	~CGame();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CPause * GetPause() { return m_Pause; };

//	static CScore*GetScore() { return pScore; };

	static CStage* GetStage() { return m_Stage; }
	static int GetGameScore() { return m_GameScore; };
	static CMagicBox* GetMagicBox() { return m_MagicBox; }

	void SetPatternStage(CStage::PATTERN Pattern) { m_pattern = Pattern; }
	static CParticleManager* GetParticleManager() { return m_PaticleManager; }
private:
	static CStage* m_Stage;
	static CPause *m_Pause;


	static int m_GameScore;
	CStage::PATTERN m_pattern;		//パターン
	static CMagicBox* m_MagicBox;
	int m_GameCount;
	int m_SpeedUp;
	int m_nCntSpawn;
	static CParticleManager* m_PaticleManager;	// パーティクルマネジャー

};
#endif