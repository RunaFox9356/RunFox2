//=============================================================================
//
// 説明書
// Author : hamada ryuuga
//
//=============================================================================


#ifndef _MOVEMESH_H_			// このマクロ定義がされてなかったら
#define _MOVEMESH_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "mesh.h"

class Spline;
class CLineMesh;
class CMoveMesh : public CMesh
{
public:

	CMoveMesh();
	~CMoveMesh() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CMoveMesh* Create();

	void AddPoptime(const D3DXVECTOR3 IsPos);
	void SetPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos, const bool IsReverse);
	D3DXVECTOR3 GetPoptime(const int IsPoptime) { return m_poptime.at(IsPoptime); }

	int GetPoptimeSize() { return m_poptime.size(); }

	void SetAnimationSpeed(const int IsSpeed) { m_animationSpeed = IsSpeed; }

	bool GetReverse(const int IsPoptime) { return m_reverse.at(IsPoptime); }



	int GetAnimationSpeed() { return m_animationSpeed; }
private:
	void move();
	void OnHit() override;

	bool CollisionWall(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld,int line);
	D3DXVECTOR3 m_testrot;
	D3DXVECTOR3 m_movemesh;
	std::vector <D3DXVECTOR3> m_poptime;
	std::vector <bool> m_reverse;
	int m_nowAnimation;
	int m_animationSpeed;
	int m_speedCount;
	bool m_fastHit;
	CLineMesh* m_lineMesh;
};
#endif

