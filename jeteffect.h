//=============================================================================
//
// effectオブジェクト
// Author : hamada ryuuga
//
//=============================================================================


#ifndef _JETEFFECT_H_			// このマクロ定義がされてなかったら
#define _JETEFFECT_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "3dpolygon.h"
#include "texture.h"

class CJetEffect : public C3dpolygon
{
public:


	static CJetEffect *CJetEffect::Create(D3DXVECTOR3 Move);

	CJetEffect(const int list);
	~CJetEffect() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const override;
	void SetPos(const D3DXVECTOR3 &pos) override;
	void SetLife(const int &Life) { m_life = Life; };

private:
	void move();
	int m_life;
};

#endif
