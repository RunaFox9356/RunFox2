//============================
//
// エフェクト設定
// Author:hamada ryuuga
//
//============================

#include "moveeffect.h"
#include "hamada.h"
#include "manager.h"
#include "utility.h"


//------------------------------------
// コンストラクタ
//------------------------------------
CMoveEffect::CMoveEffect(int list) :C3dpolygon(list)
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CMoveEffect::~CMoveEffect()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CMoveEffect::Init()
{
	m_life = 100;

	C3dpolygon::Init();
	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CMoveEffect::Uninit()
{
	C3dpolygon::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CMoveEffect::Update()
{

	//動き
	CMoveEffect::move();

	C3dpolygon::Update();

}

//------------------------------------
// 描画
//------------------------------------
void CMoveEffect::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	//アルファブレンディングを加算合成に設定
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

	//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
	//m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, m_rot);
	C3dpolygon::Draw();

	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//------------------------------------
// create
//------------------------------------
CMoveEffect *CMoveEffect::Create(D3DXVECTOR3 Move)
{
	CMoveEffect * pObject = nullptr;
	pObject = new CMoveEffect(PRIORITY_POPEFFECT);

	if (pObject != nullptr)
	{
		float Mode = FloatRandom(10, 1);
		float Speed = FloatRandom(3, 0);
		pObject->Init();
		pObject->SetTexture(CTexture::TEXTURE_SMOKE);//テクスチャ選択
		pObject->SetSize(D3DXVECTOR3(30.0f, 30.0f, 0.0f));//サイズ設定
		//pObject->SetPos(D3DXVECTOR3(0.0f, 0.0f, 0.0f));//座標設定
		pObject->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));//色設定
	
		pObject->SetMove(D3DXVECTOR3(-Move.x*Speed, 0.0f, -Move.z*Speed));//moveの設定
		pObject->SetLife(IntRandom(30, 10));
		//↓引数(1横の枚数,2縦の枚数,3Animation速度,４基本ゼロだけど表示するまでのタイムラグ,5無限にアニメーション再生するかどうか)
		//pObject->SetAnimation(7, 7, 4, 0, false);//Animation画像だった場合これを書く,一枚絵なら消さないとバグる
	}

	return pObject;
}

//------------------------------------
// Get＆Set 
//------------------------------------
const D3DXVECTOR3 * CMoveEffect::GetPos() const
{
	return &m_pos;
}

void CMoveEffect::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;
}

//------------------------------------
// 動き系統
//------------------------------------
void CMoveEffect::move()
{
	m_pos += m_move;
	//動き入れたいときはここに	SetMove()で変えれるよ
	m_life--;

	if (m_life <= 0)
	{
		Uninit();
	}
}

