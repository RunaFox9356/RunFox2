//=============================================================================
//
// 反応設定
// Author : hamada ryuuga
//
//=============================================================================


#ifndef _CUTIN_H_			// このマクロ定義がされてなかったら
#define _CUTIN_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "object2d.h"
#include "texture.h"

class CObject;

class CCutin : public CObject2d
{
public:
	const float POPSIZE = 150.0f;//基本のsize
	const float DEFAULTSIZE = 100.0f;//基本のsize
	const int CONTRACTION = 3;//収縮回数
	static CCutin *CCutin::Create(bool boost);

	CCutin(const int list);
	~CCutin() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const;
	void SetPos(const D3DXVECTOR3 &pos) override;


private:
	void move();
	bool m_pop;
	float m_size;
	int m_count;

};

#endif

