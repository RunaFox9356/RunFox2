//============================
//
// メッシュ設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MESH_H_
#define _MESH_H_

#include "main.h"
#include "object.h"

#define	EMESHX	(1)
#define	EMESHY	(5)
#define MAX_SIZEMESH (100.0f)
#define MAX_EMESH (2)
#define MOUNTAIN (50.0f)
#define MAXMOVE (10)

#define EMESHMAX (12800)


class CMesh : public CObject
{
public:
	virtual void OnHit() {}//メッシュの当たった時の判定

	CMesh(int nPriority = PRIORITY_OBJECT);
	~CMesh() override;

	HRESULT Init()override;//初期化
	void Uninit()override;//破棄
	void Update()override;//更新
	void Draw()override;//描画


	static CMesh* Create();

	bool CollisionMesh(D3DXVECTOR3 *pPos);//メッシュの当たり判定つける
	bool CollisionMeshModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPopPos);
	void Loadfile(const char * pFileName);//メッシュの読み込み

	
	//セッター
	void SetPos(const D3DXVECTOR3 &pos);
	void SetMove(float ismove) { m_move = ismove; }
	void SetMesh(const int Size);
	void SetNumber(int IsNumber) { m_number = IsNumber; }
	void SetType(int IsType) { m_type = IsType; }
	void SetTexture(const char * pFileName);

	void SetOneMeshSize(D3DXVECTOR3 IsSize) 
	{
		m_meshSize = IsSize; 
		CMesh::SetMesh(m_nowMesh);
	}
	void SwitchCollision(bool onCollision) { m_iscollision = onCollision; };

	//ゲッター
	const D3DXVECTOR3 * CMesh::GetPos() const;
	D3DXVECTOR3 GetOneMeshSize() { return m_meshSize; }
	D3DXVECTOR3 GetMeshSize() { return D3DXVECTOR3(m_X *m_meshSize.x, 0.0f, m_Z *m_meshSize.z); }
	D3DXVECTOR3 * GetPos() { return &m_posOrigin; }
	float GetMove() { return m_move; }
	int GetNumber() { return m_number; }
	int GetMeshType() { return m_type; }

	int GetMeshSizeX() { return m_X; }

	std::string GetDataNeme() { return m_dataName; }

private:
	void SetVtxMesh(VERTEX_3D* pVtx, WORD* pIdx, int nCnt, bool isUp);
	void SetVtxMeshSize(int Size);
	void SetVtxMeshLight();

	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;	    // 頂点バッファーへのポインタ
	LPDIRECT3DTEXTURE9 m_pTextureEmesh;        //テクスチャのポインタ
	LPDIRECT3DINDEXBUFFER9 m_pIdxBuff;         //インデックスバッファ

	D3DXVECTOR3 m_pos;	// 頂点座標
	D3DXVECTOR3 m_posOrigin;	// 頂点座標
	D3DXVECTOR3 m_rot;	// 回転座標
	D3DXMATRIX m_mtxWorld;// ワールドマトリックス
	int m_xsiz;//面数
	int m_zsiz;//面数
	int m_X;//辺の頂点数
	int m_Z;//辺の頂点数
	int m_vtx;//頂点数
	int m_index; //インデックス
	int m_por;
	int m_nowMesh;
	int m_number;
	int m_type;
	float m_move;
	D3DXVECTOR3 m_meshSize;
	D3DXVECTOR3* m_posMesh;
	std::string  m_fileName;
	std::string  m_dataName;
	bool m_iscollision;

	
	

};
#endif

