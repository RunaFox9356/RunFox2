//=============================================================================
//
// アニメーションを再生するやつ
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _CAMERA_PLAYER_H_
#define	_CAMERA_PLAYER_H_
#include"building.h"

class CLine;
//**************************************************
// クラス
//**************************************************
class CCamera_Player : public CBuilding
{
public:
	CCamera_Player();
	~CCamera_Player() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CCamera_Player *Create(D3DXVECTOR3 pos, D3DXVECTOR3 rot,std::string pass );
	void Move();
	void quat() {}
	void Hit() override;
	void NotHit() override;
	void Setline();

	void SetPass(std::string Pass) { animationpass = Pass; };
	std::string GetPass() { return animationpass; };
private:
	bool m_hit;
	bool m_isMove;

	std::string animationpass;
	CLine*Line[12];
};

#endif // !_PENBULUM_H_
