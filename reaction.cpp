//============================
//
// 反応設定
// Author:hamada ryuuga
//
//============================

#include "reaction.h"
#include "hamada.h"
#include "manager.h"
#include "utility.h"

//------------------------------------
// コンストラクタ
//------------------------------------
CReaction::CReaction(int list) :C3dpolygon(list)
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CReaction::~CReaction()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CReaction::Init()
{
	m_count = 0;
	m_pop = true;
	C3dpolygon::Init();
	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CReaction::Uninit()
{
	C3dpolygon::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CReaction::Update()
{
	m_rot.z += 0.01f;
	NormalizeAngle(&m_rot.z);

	if (m_pop)
	{
		m_size-=5;
		
		if (m_size <= DEFAULTSIZE)
		{
			m_count++;
			m_size = POPSIZE;
			if (m_count >= CONTRACTION)
			{
				m_size = DEFAULTSIZE;
				m_count = 0;
				m_pop = false;
			}
			
		}
		SetSize(D3DXVECTOR3(m_size, m_size, 0.0f));
	}
	//動き
	//CReaction::move();

	C3dpolygon::Update();

}

//------------------------------------
// 描画
//------------------------------------
void CReaction::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	//アルファブレンディングを加算合成に設定
	//pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	//pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	//pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	//Ｚ軸で回転しますちなみにm_rotつかうとグルグル回ります
	//m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, m_rot);
	C3dpolygon::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//------------------------------------
// create
//------------------------------------
CReaction *CReaction::Create()
{
	CReaction * pObject = nullptr;
	pObject = new CReaction(PRIORITY_POPEFFECT);

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->SetTexture(CTexture::TEXTURE_GON);//テクスチャ選択
		pObject->SetSize(D3DXVECTOR3(640.0f, 360.0f, 0.0f));//サイズ設定
		pObject->SetPos(D3DXVECTOR3(0.0f, 0.0f, 10200.0f));//座標設定
		pObject->SetColar(D3DXCOLOR(1.0f, 1.0f, 0.0f, 0.5f));//色設定
		pObject->SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));//moveの設定

														//↓引数(1横の枚数,2縦の枚数,3Animation速度,４基本ゼロだけど表示するまでのタイムラグ,5無限にアニメーション再生するかどうか)
		//pObject->SetAnimation(7, 1, 0, 0, false);//Animation画像だった場合これを書く,一枚絵なら消さないとバグる
	}

	return pObject;
}

//------------------------------------
// Get＆Set 
//------------------------------------
const D3DXVECTOR3 * CReaction::GetPos() const
{
	return &m_pos;
}

void CReaction::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;
}

//------------------------------------
// 出るときつくる
//------------------------------------
void CReaction::PopEvent(CObject* Enemy)
{
	if (m_enemy != Enemy)
	{
		
		if (!m_pop)
		{
			m_size = POPSIZE;
			SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
			SetSize(D3DXVECTOR3(POPSIZE, POPSIZE, 0.0f));
		}
		
		m_enemy = Enemy;
		m_pop = true;
		m_count = 0;
	}
}



//------------------------------------
// 動き系統
//------------------------------------
void CReaction::move()
{
	//動き入れたいときはここに	SetMove()で変えれるよ

	//m_pos += m_Move;
}

