//=============================================================================
//
// リザルト画面
// Author:hamada ryuuga
//
//=============================================================================
//-----------------------------------------------------------------------------
// iinclude
//-----------------------------------------------------------------------------
#include "result.h"
#include "input.h"
#include "manager.h"
#include "fade.h"
#include "sound.h"
#include "motion.h"
#include "stage.h"
#include "camera.h"
#include "utility.h"
#include "jeteffect.h"
#include "moveeffect.h"
//=============================================================================
// コンストラクタ
//=============================================================================
CResult::CResult()
{
}

//=============================================================================
// デストラクタ
//=============================================================================
CResult::~CResult()
{
}

//=============================================================================
// 初期化処理
//=============================================================================
HRESULT CResult::Init(void)
{

	m_Pos = 0.0f;
	
	m_Stage = CStage::Create(CStage::PATTERN_RESULT);
	CManager::GetInstance()->GetRenderer()->GetCamera()->SetPos(D3DXVECTOR3(265.763f, 582.075f, 161.255f));
	CManager::GetInstance()->GetRenderer()->GetCamera()->SetPosR(D3DXVECTOR3(-194.238f, 264.999f, 661.256f));

	////GonFoxのTITLE文字
	m_object2d[1] = CObject2d::Create(PRIORITY_UI);
	m_object2d[1]->SetTexture(CTexture::TEXTURE_LOOKON);
	m_object2d[1]->SetSize(D3DXVECTOR3(250.0f, 250.0f, 0.0f));
	m_object2d[1]->SetPos(D3DXVECTOR3(CManager::centerPos.x, 250.0f, 0.0f));
	m_object2d[1]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

	m_object2d[0] = CObject2d::Create(PRIORITY_UI);
	m_object2d[0]->SetTexture(CTexture::TEXTURE_RESULT_GON);
	m_object2d[0]->SetSize(CManager::centerPos);
	m_object2d[0]->SetPos(D3DXVECTOR3(CManager::centerPos.x, m_Pos, CManager::centerPos.z));
	m_object2d[0]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));




	CManager::GetInstance()->GetSound()->Play(CSound::LABEL_BGM_RESET);
	m_Rot = 0.0f;
	m_Colar = 0.0f;

	m_On = false;
	return S_OK;
}

//=============================================================================
// 終了処理
//=============================================================================
void CResult::Uninit(void)
{
	CManager::GetInstance()->GetSound()->Stop();
	CObject::Release();
}

//=============================================================================
// 更新処理
//=============================================================================
void CResult::Update(void)
{
	m_Pos+=3;
	if (m_Pos >= CManager::centerPos.y)
	{
		if (!m_On)
		{
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_ON);
		}
		m_On = true;

		m_Pos = CManager::centerPos.y;
		m_Colar += 0.05;
		if (m_Colar >= 1.0f)
		{
			m_Colar = 1.0f;
		}
		m_object2d[1]->SetColor(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_Colar));
	}
	m_object2d[0]->SetPos(D3DXVECTOR3(CManager::centerPos.x, m_Pos, CManager::centerPos.z));
	float R = FloatRandom(1.0f, 0.0f);
	float G = FloatRandom(1.0f, 0.0f);
	float B = FloatRandom(1.0f, 0.0f);
	float MoveX = FloatRandom(10.0f, -10.0f);
	float MoveY = FloatRandom(15.0f, 10.0f);
	float MoveZ = FloatRandom(10.0f, -10.0f);

	CMoveEffect*JetEffect = CMoveEffect::Create({ MoveX ,MoveY ,MoveZ });
	JetEffect->SetSize(D3DXVECTOR3(200.0f, 0.0f, 200.0f));//サイズ設定
	JetEffect->SetColar(D3DXCOLOR(R, G, B, 1.0f));//色設定

	JetEffect->SetPos(D3DXVECTOR3(-220.0f, 110.0f, 520.0f));

	m_Rot += 0.05f;

	NormalizeAngle(&m_Rot);

	m_object2d[1]->SetRot(D3DXVECTOR3(0.0f, 0.0f, m_Rot));


	CInput *CInputpInput = CInput::GetKey();
	if (CInputpInput->Trigger(CInput::KEY_DECISION))
	{
		//モードの設定
		CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_NAMESET);
	}
#ifdef _DEBUG

	if (CInputpInput->Trigger(CInput::KEY_DEBUG))
	{
		//モードの設定
		CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_TITLE);
	}

#endif // DEBUG
}

//=============================================================================
// 描画処理
//=============================================================================
void CResult::Draw(void)
{

}