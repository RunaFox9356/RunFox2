//============================
//
// 動くmesh設定
// Author:hamada ryuuga
//
//============================

#include "runmesh.h"
#include "utility.h"
#include "stage.h"
#include "player.h"
#include "title.h"
#include "game.h"

//------------------------------------
// コンストラクタ
//------------------------------------
CRunMesh::CRunMesh()
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CRunMesh::~CRunMesh()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CRunMesh::Init()
{
	CMesh::Init();

	m_testrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	return S_OK;
}

//------------------------------------
// 終了
//------------------------------------
void CRunMesh::Uninit()
{
	CMesh::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CRunMesh::Update()
{
	CMesh::Update();
	//動き
	CRunMesh::move();
	//.CRunMesh::OnHit();
}

//------------------------------------
// 描画
//------------------------------------
void CRunMesh::Draw()
{
	CMesh::Draw();
}

//------------------------------------
// create
//------------------------------------
CRunMesh *CRunMesh::Create()
{
	CRunMesh * pObject = new CRunMesh;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}


//------------------------------------
// 動き系統
//------------------------------------
void CRunMesh::move()
{
	
}

//------------------------------------
// Playerが当たった時の判定
//------------------------------------
void CRunMesh::OnHit()
{
	m_movemesh = CGame::GetStage()->GetPlayer()->GetMove();

	m_movemesh.x -= GetMove();

	CGame::GetStage()->GetPlayer()->SetMove(m_movemesh);

	CGame::GetStage()->GetPlayer()->SetFriction(-0.2f);
}
