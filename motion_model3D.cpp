//=============================================================================
//
// モーションキャラクター3Dクラス(model3D.h)
// Author : hamada ryuuga hamada ryuuga
// 概要 : モーションキャラクター3D生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "motion_model3D.h"
#include "motion.h"
#include "renderer.h"
#include "manager.h"

//=============================================================================
// インスタンス生成
// Author : hamada ryuuga hamada ryuuga
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CMotionModel3D * CMotionModel3D::Create()
{
	// オブジェクトインスタンス
	CMotionModel3D *pMotionModel3D = nullptr;

	// メモリの解放
	pMotionModel3D = new CMotionModel3D;

	// メモリの確保ができなかった
	assert(pMotionModel3D != nullptr);

	// 数値の初期化
	pMotionModel3D->Init();

	// インスタンスを返す
	return pMotionModel3D;
}

//=============================================================================
// コンストラクタ
// Author : hamada ryuuga hamada ryuuga
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CMotionModel3D::CMotionModel3D(int nPriority) : CObject(nPriority),
m_pMotion(nullptr),		// モーション情報
m_mtxWorld(D3DXMATRIX()),									// ワールドマトリックス
m_pos(D3DXVECTOR3()),										// 位置
m_posOld(D3DXVECTOR3()),									// 過去位置
m_rot(D3DXVECTOR3()),										// 向き
m_size(D3DXVECTOR3())										// 大きさ
{
	
}

//=============================================================================
// デストラクタ
// Author : hamada ryuuga hamada ryuuga
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CMotionModel3D::~CMotionModel3D()
{

}

//=============================================================================
// 初期化
// Author : hamada ryuuga hamada ryuuga
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CMotionModel3D::Init()
{
	// 変数の初期化
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);					// 位置
	m_posOld = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 過去位置
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);					// 向き
	m_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);				// スケール
	m_size = D3DXVECTOR3(1.0f, 1.0f, 1.0f);					//当たり判定
	m_quat = { 0.0f,0.0f,0.0f,1.0f };
	SetIsColor(false);
	SetColor({ 1.0f,1.0f,1.0f,1.0f });
	D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXQuaternionRotationAxis(&m_quat, &vecY, D3DX_PI);
	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : hamada ryuuga hamada ryuuga
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CMotionModel3D::Uninit()
{
	if (m_pMotion != nullptr)
	{// 終了処理
		m_pMotion->Uninit();

		// メモリの解放
		delete m_pMotion;
		m_pMotion = nullptr;
	}

	// オブジェクト2Dの解放
	Release();
}

//=============================================================================
// 更新
// Author : hamada ryuuga hamada ryuuga
// 概要 : 2D更新を行う
//=============================================================================
void CMotionModel3D::Update()
{
	if (m_pMotion != nullptr)
	{// モーション番号の設定
		m_pMotion->Update();
	}
}

//=============================================================================
// 描画
// Author : hamada ryuuga hamada ryuuga
// 概要 : 2D描画を行う
//=============================================================================
void CMotionModel3D::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();

	if (m_Stencil)
	{
		// ステンシルバッファ -> 有効
		pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

		// ステンシルバッファと比較する参照値設定 -> ref
		pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);

		// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
		pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

		// ステンシルテストの比較方法 ->
		// （参照値 >= ステンシルバッファの参照値）なら合格
		pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_GREATEREQUAL);

		// ステンシルテストの結果に対しての反映設定
		pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);		// Zとステンシル成功
		pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
		pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);			// Zのみ失敗
	}
	else
	{
		// ステンシルバッファ -> 無効
		pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
	}

	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxRotQuat, mtxScale;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);											// 行列初期化関数

	// クォータニオンの使用した姿勢の設定
	D3DXMatrixRotationQuaternion(&mtxRotQuat, &m_quat);            // クオータニオンによる行列回転
	
	D3DXMatrixScaling(&mtxScale, m_scale.y, m_scale.x, m_scale.z);			// スケール拡縮行列
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScale);				// 行列掛け算関数 

	// 向きの反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);			// 行列回転関数

	D3DXMatrixMultiply(&mtxRot, &mtxRot, &mtxRotQuat);						// 行列掛け算関数 

	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);						// 行列掛け算関数 
	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);				// 行列移動関数
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);					// 行列掛け算関数

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	if (m_pMotion != nullptr)
	{// パーツの描画設定
		m_pMotion->SetParts(m_mtxWorld, m_Iscolor, m_color);
	}
	// ステンシルバッファ -> 無効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}

//=============================================================================
// モーションの設定
// Author : hamada ryuuga hamada ryuuga
// 概要 : モーションの読み込みを行う
//=============================================================================
void CMotionModel3D::SetMotion(const char * pName)
{
	if (m_pMotion != nullptr)
	{// 終了処理
		m_pMotion->Uninit();

		// メモリの解放
		delete m_pMotion;
		m_pMotion = nullptr;
	}

	// モーション情報
	m_pMotion = new CMotion(pName);
	assert(m_pMotion != nullptr);

	// モーションの初期設定
	m_pMotion->SetMotion(0);

	// モーション番号の設定
	m_pMotion->SetNumMotion(0);
}

//=============================================================================
// 色変えるの設定
// Author : hamada ryuuga
// 概要 : モデルの色変える
//=============================================================================
void CMotionModel3D::ChangeColar(D3DXCOLOR color)
{
	SetColor(color);
	SetIsColor(true);
}
