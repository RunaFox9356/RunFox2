//============================
//
// 反応設定
// Author:hamada ryuuga
//
//============================

#include "cutin.h"
#include "hamada.h"
#include "manager.h"
#include "utility.h"

//------------------------------------
// コンストラクタ
//------------------------------------
CCutin::CCutin(int list) :CObject2d(list)
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CCutin::~CCutin()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CCutin::Init()
{
	m_count = 0;
	m_pop = true;
	CObject2d::Init();
	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CCutin::Uninit()
{
	CObject2d::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CCutin::Update()
{
	
	//動き
	//CCutin::move();

	CObject2d::Update();

}

//------------------------------------
// 描画
//------------------------------------
void CCutin::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();

	CObject2d::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//------------------------------------
// create
//------------------------------------
CCutin *CCutin::Create(bool boost)
{
	CCutin * pObject = nullptr;
	pObject = new CCutin(PRIORITY_POPEFFECT);

	if (pObject != nullptr)
	{
		pObject->Init();

		pObject->SetSize(D3DXVECTOR3(640.0f, 360.0f, 0.0f));//サイズ設定
		pObject->SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));//座標設定
		pObject->SetColor(D3DXCOLOR(1.0f, 1.0f, 0.0f, 0.3f));//色設定
		pObject->SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));//moveの設定
		if (boost)
		{
			pObject->SetTexture(CTexture::TEXTURE_BOOST);//テクスチャ選択
			pObject->SetAnimation(2, 10, 3, 0, false);
		}
		else
		{
			pObject->SetTexture(CTexture::TEXTURE_HISATU);//テクスチャ選択
			pObject->SetAnimation(2, 5, 5, 0, false);
		}
	

		//↓引数(1横の枚数,2縦の枚数,3Animation速度,４基本ゼロだけど表示するまでのタイムラグ,5無限にアニメーション再生するかどうか)
		//pObject->SetAnimation(7, 1, 0, 0, false);//Animation画像だった場合これを書く,一枚絵なら消さないとバグる
	}

	return pObject;
}

//------------------------------------
// Get＆Set 
//------------------------------------
const D3DXVECTOR3 * CCutin::GetPos() const
{
	return &m_pos;
}

void CCutin::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;
}

//------------------------------------
// 動き系統
//------------------------------------
void CCutin::move()
{
	//動き入れたいときはここに	SetMove()で変えれるよ

	//m_pos += m_Move;
}

