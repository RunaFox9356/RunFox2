//============================
//
// メッシュ設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _LINEMESH_H_
#define _LINEMESH_H_

#include "main.h"
#include "object.h"
#include "line.h"

class CLine;
class CLineMesh : public CObject
{
public:
	const int AnimationSpeed = 30;
	const int MaxVtx = 2000;

	CLineMesh(int nPriority = PRIORITY_OBJECT);
	~CLineMesh() override;

	HRESULT Init()override;//初期化
	void Uninit()override;//破棄
	void Update()override;//更新
	void Draw()override;//描画
	void OnHit();
	void NoHit() { m_fastHit = false; }
	bool CollisionMesh(D3DXVECTOR3 *pPos);

	static CLineMesh* Create();

	//法線や座標設定
	void VtxCreate(D3DXVECTOR3 * Pos, D3DXVECTOR3 * PosOld,int num);

	void VtxChange();

	//当たり判定つかうかどうか
	void SwitchCollision(bool onCollision) { m_iscollision = onCollision; };

	//ゲッター
	const D3DXVECTOR3 *GetPos() const;
	D3DXVECTOR3 GetOneMeshSize() { return m_meshSize; }
	D3DXVECTOR3 * GetPos() { return &m_posOrigin; }

	//	おおもとのポイントを追加
	void AddPoptime(const D3DXVECTOR3 IsPos) { m_poptime.push_back(IsPos); }
	//	おおもとのポイントをセット
	void SetPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_poptime.at(IsPoptime) = IsPos; }
	//	おおもとのポイントをゲット
	D3DXVECTOR3 GetPoptime(const int IsPoptime) { return m_poptime.at(IsPoptime); }
	//	おおもとのポイントのさいずの取得
	int GetPoptimeSize() { return m_poptime.size(); }

	//	反転情報をゲット
	bool GetReverse(const int IsPoptime) { return m_reverse.at(IsPoptime); }
	//	反転情報をセット
	void SetReverse(const int IsPoptime, const bool IsReverse) { m_reverse.at(IsPoptime) = IsReverse; }
	//	反転情報を追加
	void AddReverse(bool IsReverse) { m_reverse.push_back(IsReverse); }

	//	アニメーションする細かい座標設定追加
	void AnimationAddPoptime(const D3DXVECTOR3 IsPos) { m_animationPoptime.push_back(IsPos); }
	//	ニメーションする細かい座標設定セット
	void AnimationSetPoptime(const int IsPoptime, const D3DXVECTOR3 IsPos) { m_animationPoptime.at(IsPoptime) = IsPos; }
	//	ニメーションする細かい座標設定ゲット
	D3DXVECTOR3 AnimationGetPoptime(const int IsPoptime) { return m_animationPoptime.at(IsPoptime); }
	// ニメーションする細かい座標設定のさいずの取得
	int AnimationGetPoptimeSize() { return m_animationPoptime.size(); }
	
	//当たり判定
	bool CollisionLine(D3DXVECTOR3 *VecMeshCurrent, D3DXVECTOR3 *VecMeshNext, D3DXVECTOR3 *VecMeshCurrent1, D3DXVECTOR3 *VecMeshNext1);

private:

	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);

	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;	    // 頂点バッファーへのポインタ
	LPDIRECT3DTEXTURE9 m_pTextureEmesh;        //テクスチャのポインタ
	LPDIRECT3DINDEXBUFFER9 m_pIdxBuff;         //インデックスバッファ

	D3DXVECTOR3 m_pos;	// 頂点座標
	D3DXVECTOR3 m_posOrigin;	// 頂点座標
	D3DXVECTOR3 m_rot;	// 回転座標
	D3DXMATRIX m_mtxWorld;// ワールドマトリックス
	D3DXVECTOR3 m_meshSize;
	D3DXVECTOR3* m_posMesh;

	int m_maxVtx;//頂点数
	int m_nowVtx;//頂点数
	int m_nowAnimation;
	int m_animationSpeed;
	int m_speedCount;

	
	std::vector <D3DXVECTOR3> m_poptime;
	std::vector <bool> m_reverse;
	std::vector <D3DXVECTOR3> m_animationPoptime;

	bool m_fastHit;
	bool m_upMove;
	bool m_iscollision;

	CLine*Line[2000];

};
#endif


