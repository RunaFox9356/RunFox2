//=============================================================================
//
// 3Dモーションキャラクタークラス(motion_model3D.h)
// Author : hamada ryuuga
// 概要 : 3Dプレイヤー生成を行う
//
//=============================================================================
#ifndef _MOTION_MODEL3D_H_			// このマクロ定義がされてなかったら
#define _MOTION_MODEL3D_H_			// 二重インクルード防止のマクロ定義

//*****************************************************************************
// インクルード
//*****************************************************************************
#include "object.h"
#include "main.h"

//*****************************************************************************
// 前方宣言
//*****************************************************************************
class CMotion;

//=============================================================================
// 3Dプレイヤークラス
// Author : hamada ryuuga
// 概要 : 3Dプレイヤー生成を行うクラス
//=============================================================================
class CMotionModel3D : public CObject
{
public:
	//--------------------------------------------------------------------
	// 静的メンバ関数
	//--------------------------------------------------------------------
	static CMotionModel3D *Create();			// 3Dプレイヤーの生成

	//--------------------------------------------------------------------
	// コンストラクタとデストラクタ
	//--------------------------------------------------------------------
	explicit CMotionModel3D(int nPriority = 0);
	~CMotionModel3D();

	//--------------------------------------------------------------------
	// メンバ関数
	//--------------------------------------------------------------------
	HRESULT Init() override;														// 初期化
	void Uninit() override;															// 終了
	void Update() override;															// 更新
	void Draw() override;															// 描画
	void SetPos(const D3DXVECTOR3 &pos)  { m_pos = pos; };					// 位置のセッター
	void SetPosOld(const D3DXVECTOR3 &posOld)  { m_posOld = posOld; }		// 過去位置のセッター
	void SetRot(const D3DXVECTOR3 &rot)  { m_rot = rot; };					// 向きのセッター
	void SetScale(const D3DXVECTOR3 &scale) { m_scale = scale; }			// スケールのセッター
	void SetSize(const D3DXVECTOR3 &size)  { m_size = size; }				// 当たり判定のセッター
	D3DXVECTOR3 GetPos()  { return m_pos; }									// 位置のゲッター
	D3DXVECTOR3 GetPosOld()   { return m_posOld; }							// 過去位置のゲッター
	D3DXVECTOR3 GetRot()   { return m_rot; }								// 向きのゲッター
	D3DXVECTOR3 GetScale() { return m_scale; }								// スケールのゲッター
	D3DXVECTOR3 GetSize()   { return m_size; }								// 当たり判定のゲッター
	void SetMtxWorld(D3DXMATRIX mtxWorld) { m_mtxWorld = mtxWorld; }				// ワールドマトリックスのセッター
	D3DXMATRIX* GetMtxWorld() { return &m_mtxWorld; }									// ワールドマトリックスのゲッター
	void SetMotion(const char *pName);												// モーション情報の設定
	CMotion *GetMotion() { return m_pMotion; }										// モーション情報の取得

	void SetStencil(bool isStencil) { m_Stencil = isStencil; }
	D3DXQUATERNION GetQuat() { return m_quat; }
	void  SetQuat(D3DXQUATERNION quat) { m_quat = quat; }

	D3DXCOLOR GetColor() { return m_color; }
	void SetColor(D3DXCOLOR iscolor) { m_color = iscolor;}

	bool GetIsColor() { return m_Iscolor; }
	void SetIsColor(bool iscolor) { m_Iscolor = iscolor; }


	void ChangeColar(D3DXCOLOR color);
private:
	//--------------------------------------------------------------------
	// メンバ変数
	//--------------------------------------------------------------------
	CMotion			*m_pMotion;				// モーションインスタンス
	D3DXMATRIX		m_mtxWorld;				// ワールドマトリックス
	D3DXVECTOR3		m_pos;					// 位置
	D3DXVECTOR3		m_posOld;				// 過去位置
	D3DXVECTOR3		m_rot;					// 向き
	D3DXVECTOR3		m_scale;				// スケール
	D3DXVECTOR3		m_size;					// 当たり判定
	D3DXCOLOR		m_color;				// 色
	D3DXQUATERNION m_quat;
	bool m_Stencil;
	bool m_Iscolor;
};

#endif