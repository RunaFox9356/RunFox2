//=============================================================================
//
// オブジェクトクラス(object.h)
// Author : 唐�ｱ結斗
// 概要 : オブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>

#include "gauge3D.h"
#include "object.h"
#include "renderer.h"


//=============================================================================
// インスタンス生成
// Author : 唐�ｱ結斗
// 概要 : 2Dオブジェクトを生成する
//=============================================================================
CGauge3D * CGauge3D::Create(void)
{
	// オブジェクトインスタンス
	CGauge3D *pPolygon2D = nullptr;

	// メモリの解放
	pPolygon2D = new CGauge3D;

	if (pPolygon2D != nullptr)
	{// 数値の初期化
		pPolygon2D->Init();
	}
	else
	{// メモリの確保ができなかった
		assert(false);
	}

	// インスタンスを返す
	return pPolygon2D;
}

//=============================================================================
// コンストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CGauge3D::CGauge3D()
{

}

//=============================================================================
// デストラクタ
// Author : 唐�ｱ結斗
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CGauge3D::~CGauge3D()
{
	
}

//=============================================================================
// 更新
// Author : 唐�ｱ結斗
// 概要 : 2D更新を行う
//=============================================================================
void CGauge3D::Update()
{

}

//=============================================================================
// 頂点座標などの設定
// Author : 唐�ｱ結斗
// 概要 : 2Dポリゴンの頂点座標、rhw、頂点カラーを設定する
//=============================================================================
void CGauge3D::SetVtx()
{
	
}
