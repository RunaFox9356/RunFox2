//============================
//
// 影設定
// Author:hamada ryuuga
//
//============================

#include "shadow.h"
#include "hamada.h"
#include "manager.h"
#include "player.h"
#include "stage.h"
#include "game.h"

//------------------------------------
// コンストラクタ
//------------------------------------
CShadow::CShadow(int list) :C3dpolygon(list)
{
}

//------------------------------------
// デストラクタ
//------------------------------------
CShadow::~CShadow()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT CShadow::Init()
{

	D3DXQuaternionIdentity(&m_quat);
	D3DXVECTOR3 vecY = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXQuaternionRotationAxis(&m_quat, &vecY, D3DX_PI);
	C3dpolygon::Init();
	return E_NOTIMPL;
}

//------------------------------------
// 終了
//------------------------------------
void CShadow::Uninit()
{
	C3dpolygon::Uninit();
}

//------------------------------------
// 更新
//------------------------------------
void CShadow::Update()
{

	//動き
	CShadow::move();

	C3dpolygon::Update();
	
}

//------------------------------------
// 描画
//------------------------------------
void CShadow::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetInstance()->GetRenderer()->GetDevice();
	//アルファブレンディングを加算合成に設定
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);

	//m_mtxWorld = *hmd::giftmtx(&m_mtxWorld, m_pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_mtxWorld = *hmd::giftmtxQuat(&m_mtxWorld, m_pos, m_quat);

	C3dpolygon::Draw();

	//αブレンディングを元に戻す
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
}

//------------------------------------
// create
//------------------------------------
CShadow *CShadow::Create(D3DXVECTOR3 Pos)
{
	CShadow * pObject = nullptr;
	pObject = new CShadow(PRIORITY_EFFECT);

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->SetTexture(CTexture::TEXTURE_SHADOW);//テクスチャ選択
		pObject->SetSize(D3DXVECTOR3(100.0f, 0.0f, 100.0f));//サイズ設定
		//動き入れたいときはここに	SetMove()で変えれるよ
		pObject->SetPos(D3DXVECTOR3(Pos.x, Pos.y + 3.0f, Pos.z));//座標設定
		pObject->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));//色設定
		pObject->SetMove(D3DXVECTOR3(0.0f, 0.0f, 0.0f));//moveの設定

		//↓引数(1横の枚数,2縦の枚数,3Animation速度,４基本ゼロだけど表示するまでのタイムラグ,5無限にアニメーション再生するかどうか)
		//pObject->SetAnimation(7, 1, 0, 0, false);//Animation画像だった場合これを書く,一枚絵なら消さないとバグる
	}

	return pObject;
}

//------------------------------------
// Get＆Set 
//------------------------------------
const D3DXVECTOR3 * CShadow::GetPos() const
{
	return &m_pos;
}

void CShadow::SetPos(const D3DXVECTOR3 & pos)
{
	m_pos = pos;
}



void CShadow::move()
{
}

