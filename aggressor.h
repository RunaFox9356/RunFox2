//=============================================================================
//
// dashitem
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _AGGRESSOR_H_
#define	_AGGRESSOR_H_

#include"enemy.h"

class CReaction;
class CShadow;
//**************************************************
// クラス
//**************************************************
class  CAggressor : public CEnemy
{
public:
	const float	TARGET_RADIUS = 150.0f;										//ターゲットの半径
	const int AIMEING = 120;
	const int MOVE = 30;
	CAggressor();
	~CAggressor() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CAggressor *Create(D3DXVECTOR3 pos);
	void Move();
	void quat() {}
	void Hit(CObject *object)override;
	void NotHit()override;


	//Model　sizeの設定
	void SetlookMin(const D3DXVECTOR3 ismin) { m_lookmin = ismin; }
	void SetlookMax(const D3DXVECTOR3 ismax) { m_lookmax = ismax; }

	D3DXVECTOR3 GetlookMin() { return m_lookmin; }
	D3DXVECTOR3 GetlookMax() { return m_lookmax; }

	void SetBossPos(D3DXVECTOR3 IsBoss) { m_bosspos = IsBoss; }

	bool GetPlayerHit() { return m_isPlayerHit; }

	bool GetThrow() { return m_throw; }
	void SetThrow(bool IsThrow) { m_throw = IsThrow; }
	void SetMove(D3DXVECTOR3 move) { m_move = move; }
private:

	bool m_aiming;
	bool m_hit;
	bool m_isMove;
	bool m_isPlayerHit;
	bool m_des;
	bool m_get;
	bool m_throw;

	int m_nouHave;
	int m_Stop;
	D3DXVECTOR3 m_move;

	CReaction *m_sensing;

	int m_lookCount;
	int m_aimingCount;

	//感知範囲
	D3DXVECTOR3 m_lookmin;
	D3DXVECTOR3 m_lookmax;

	D3DXVECTOR3 m_posOld;

	D3DXVECTOR3 m_bosspos;

	bool CollisionMove(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize);//うごくかどうかの当たり判定
	CShadow *m_shadow;
	float m_alpha;
	bool m_isChange;
};

#endif // !_PENBULUM_H_


