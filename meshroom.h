//=============================================================================
//
// メッシュの各タイプ設定
// Author : hamada ryuuga::��野未夢瞳
//
//=============================================================================


#ifndef _MESHROOM_H_			// このマクロ定義がされてなかったら
#define _MESHROOM_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "mesh.h"

class CMeshRoom : public CMesh
{
public:

	CMeshRoom();
	~CMeshRoom() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CMeshRoom* Create();
	
private:
	D3DXVECTOR3 m_testrot;
	D3DXVECTOR3 m_meshmove;

	void move();
	void OnHit() override;
};
#endif


