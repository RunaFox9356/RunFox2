//=============================================================================
//
// モデルオブジェクトクラス(model_obj.cpp)
// Author : 浜田琉雅
// 概要 : モデルオブジェクト生成を行う
//
//=============================================================================

//*****************************************************************************
// インクルード
//*****************************************************************************
#include <assert.h>
#include <stdio.h>

#include "model3D.h"
#include "bullet.h"
#include "renderer.h"
#include "camera.h"
#include "enemy.h"
#include "utility.h"

namespace
{
	const int maxLife = 100;
	const float friction = 0.5f;
}

//=============================================================================
// インスタンス生成
// Author : hamada ryuuga
// 概要 : モーションキャラクター3Dを生成する
//=============================================================================
CBullet * CBullet::Create()
{
	// オブジェクトインスタンス
	CBullet *pModelObj = nullptr;

	// メモリの解放
	pModelObj = new CBullet;

	// メモリの確保ができなかった
	assert(pModelObj != nullptr);

	// 数値の初期化
	pModelObj->Init();

	// インスタンスを返す
	return pModelObj;
}

//=============================================================================
// コンストラクタ
// Author : hamada ryuuga
// 概要 : インスタンス生成時に行う処理
//=============================================================================
CBullet::CBullet() : m_life(0), m_moveTimer(0)
{
}

//=============================================================================
// デストラクタ
// Author : hamada ryuuga
// 概要 : インスタンス終了時に行う処理
//=============================================================================
CBullet::~CBullet()
{

}

//=============================================================================
// 初期化
// Author : hamada ryuuga
// 概要 : 頂点バッファを生成し、メンバ変数の初期値を設定
//=============================================================================
HRESULT CBullet::Init()
{
	CObjectX::Init();
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_Division = false;
	SetUp(BULLET);
	SetModel("data/MODEL/Bell/suzu.x");
	return E_NOTIMPL;
}

//=============================================================================
// 終了
// Author : hamada ryuuga
// 概要 : テクスチャのポインタと頂点バッファの解放
//=============================================================================
void CBullet::Uninit()
{
	CObjectX::Uninit();

}

//=============================================================================
// 更新
// Author : hamada ryuuga
// 概要 :更新作業をする
//=============================================================================
void CBullet::Update()
{
	m_posOld = GetPos();
	CObjectX::Update();
	if (m_moveTimer <= maxLife*0.5f)
	{
		m_move = Move();
	}
	
	SetPos(GetPos()+ m_move);
	SearchModelObject(0, ENEMY, [this](CObject*Enemy)
	{
		CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
		pEnemy->CollisionModel(&GetPosOrigin(), &m_posOld, &D3DXVECTOR3(100.0f, 100.0f, 100.0f), this);
	});
	SearchModelObject(0, BOSS, [this](CObject*Enemy)
	{
		CEnemy* pEnemy = dynamic_cast<CEnemy*>(Enemy);
		pEnemy->CollisionModel(&GetPosOrigin(), &m_posOld, &D3DXVECTOR3(100.0f, 100.0f, 100.0f), this);
	});

	m_life++;

	if (m_life >= maxLife)
	{
		Uninit();
		return;
	}
}

//=============================================================================
// 描画
// Author : hamada ryuuga
// 概要 : 描画を行う
//=============================================================================
void CBullet::Draw()
{
	CObjectX::Draw();
}

//=============================================================================
// 移動
// Author : hamada ryuuga
// 概要 : 移動の処理
//=============================================================================
D3DXVECTOR3 CBullet::Move()
{
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	switch (m_type)
	{
	case CBullet::NORMAL:
		move = NormalMove();
		break;
	case CBullet::HOMING:
		move = HomingMove();
		break;
	case CBullet::SPEED:
		move = SpaceMove();
		break;
	case CBullet::DIVISION:
		move = DivisionMove();
		break;
	case CBullet::NOW_NON:
		move = NormalMove();
		break;
	case CBullet::MAX:
		break;
	default:
		break;
	}

	return move;
}

//--------------------------------------------------
// 通常弾
//--------------------------------------------------
D3DXVECTOR3 CBullet::NormalMove()
{
	return D3DXVECTOR3(0.0f, 0.0f, BulletMove);
}

//--------------------------------------------------
// ホーミング弾
//--------------------------------------------------
D3DXVECTOR3 CBullet::HomingMove()
{
	
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 Rot = GetRot();

	if (m_target == nullptr)
	{
		return D3DXVECTOR3(0.0f, 0.0f, BulletMove);
	}

	D3DXVECTOR3 vecPlayerDist = m_target->GetPos() - GetPos();
	float distPlayer = D3DXVec3Length(&vecPlayerDist);


	move += vecPlayerDist / distPlayer * 15.0f;

	Rot.y = (atan2f(vecPlayerDist.x, vecPlayerDist.z));

	SetRot(Rot);

	return move;
}

//--------------------------------------------------
//  加速弾
//--------------------------------------------------
D3DXVECTOR3 CBullet::SpaceMove()
{
	D3DXVECTOR3 move = m_move;
	

	if (move.z <= BulletMove *2.0f)
	{
		move.z += 3.0f;
	}
	
	return move;
}

//--------------------------------------------------
// 増殖
//--------------------------------------------------
D3DXVECTOR3 CBullet::DivisionMove()
{
	D3DXVECTOR3 move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	
	if (!m_Division)
	{
		D3DXVECTOR3 Diff = D3DXVECTOR3(150.0f, 0.0f, 0.0f);
		D3DXVECTOR3 Pos = GetPos();
		
		CBullet * Division1 = Create();
		Division1->SetBulletType(NORMAL);
		Division1->SetPos(Pos - Diff);

		CBullet * Division2 = Create();
		Division2->SetBulletType(NORMAL);
		Division2->SetPos(Pos + Diff);

		m_Division = true;
	}
	return D3DXVECTOR3(0.0f, 0.0f, BulletMove);
}

//--------------------------------------------------
// 当たり判定　線分
//--------------------------------------------------
bool CBullet::CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize)
{
	D3DXMATRIX mtxWorld = CObjectX::GetWorldMtx();


	bool bIsLanding = false;

	D3DXVECTOR3 min = CObjectX::GetVtxMin();
	D3DXVECTOR3 max = CObjectX::GetVtxMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			Hit();
			bIsLanding = true;
		}
	}
	else
	{
	
	}

	return bIsLanding;
}

//--------------------------------------------------
// 弾のダメージを判別
//--------------------------------------------------
int CBullet::GetDamage()
{
	switch (m_type)
	{
	case CBullet::NORMAL:
		return 20;
		break;
	case CBullet::HOMING:
		return 10;
		break;
	case CBullet::SPEED:
		return 30;
		break;
	case CBullet::DIVISION:
		return 20;
		break;
	case CBullet::MAX:
		break;
	default:
		break;
	}
	return 0;
}

void CBullet::Hit()
{
	m_target->Uninit();
}
