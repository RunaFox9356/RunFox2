//=============================================================================
//
// BOSS
// Author:hamada ryuuga
//
//=============================================================================
#ifndef _BOSS_H_
#define	_BOSS_H_

#include"enemy.h"


class CBossbar;
class CShadow;
class CObject2d;
class CHpGauge;
namespace nl = nlohmann;
//**************************************************
// クラス
//**************************************************
class  CBoss : public CEnemy
{
public:

	const float MOVESPEED = 150.0f;
	const int METEOTIME = 20;
	const int METEOMAX = 3;

	const int ATTACKTIME = 120;
	const int ATTACKMAX = 3;
	const int ATTACKBACK = 7500;

	const int METEOMOVETIME = 150;
	const int METEOMOVEMAX = 1;
	const int METEOMOVEBACK = 7500;

	// モーション
	enum EMotionType
	{
		NONE = 0,		// 通常
		METEO,			// 攻撃
		MOVE,			// 動く
		ATTACKMOVE,		// 攻撃移動
		MAXBOSS			// あんただれや？
	};

	enum EActiveType
	{
		ACTIVE_DEATH = -1,
		ACTIVE_NONE = 0,
		ACTIVE_METEO,
		ACTIVE_MOVE,
		ACTIVE_ATTACKMOVE,
		MAX_ACTIVE
	};

	static CBossbar *m_life;
	static CHpGauge *m_pHpGauge;

	CBoss();
	~CBoss() override;

	HRESULT Init() override;
	void Update() override;

	static CBoss *Create(D3DXVECTOR3 pos);
	void Hit(CObject *object)override;
	void NotHit()override;

	static void Add(int IsAdd);
	static void DesEvent();
	void SetcheckHit(bool length) { m_hit = length; m_damageEffectCnt = 120; }
private:
	void DamageEffect();

	// アクション
	void Death();
	void Wait();
	void Attack();
	void Meteo();
	void MeteoMove();

private:

	float pointCollar;
	static int m_Hp;
	int m_Meteosleep;
	int m_Meteo;
	int m_cooldown;
	int m_attack;
	int m_frameCount;
	int m_nouevent;
	int m_MeteoMovesleep;
	int m_MeteoMove;
	int m_damageEffectCnt;
	std::vector <int>m_eventList;

	bool m_isTop;
	bool m_hit;
	bool m_isMove;

	bool m_isFly;

	bool m_change;
	static bool m_DesEvent;

	D3DXVECTOR3 m_AttackMove;
	D3DXVECTOR3 m_targetPos;
	D3DXVECTOR3 m_MeteoMoveSpeed;
	D3DXVECTOR3 m_Gurabitexi;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_posOld;

	EActiveType m_active;

	CShadow *m_shadow;

	CObject2d *m_object2d;

	float m_movelog;
};

#endif // !_PENBULUM_H_


