//=============================================================================
//
// 建物
// Author : hamada ryuuga
//
//=============================================================================


#ifndef _BUILDING_H_			// このマクロ定義がされてなかったら
#define _BUILDING_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "objectX.h"
#include "texture.h"



class CBuilding : public CObjectX
{
public:

	virtual void Hit();
	virtual void NotHit() {};
	static CBuilding *CBuilding::Create(const char * pFileName, D3DXVECTOR3 *pPos);

	explicit CBuilding(int nPriority = PRIORITY_OBJECT);
	~CBuilding() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void SetGold(bool IsGo) { m_gold = IsGo; }
	void SetModelNumber(int IsNumber) { m_number = IsNumber; }
	void SetChangePoptime(bool ChangePoptime) { m_changePoptime = ChangePoptime; }

	void SetFileName(const char * pFileName) { m_fileName = pFileName; }

	std::string GetFileName() { return m_fileName; }
	bool GetGold() { return m_gold; }
	bool GetChangePoptime() { return m_changePoptime; }
	bool CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize);
	int GetModelNumber() {return m_number; }

	void SetIsMove(bool Move) { m_isMove = Move; }
	bool GetIsMove() { return m_isMove; }

	void quat(){};
private:
	void move();
	std::string m_fileName;
	D3DXVECTOR3 m_move;
	bool m_gold;
	bool m_changePoptime;
	bool m_isMove;
	int m_number;
	static int m_nowNumber;


};

#endif

