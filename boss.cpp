//=============================================================================
//
// BOSS
// Author:hamada ryuuga
//
//=============================================================================

#include "boss.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "reaction.h"
#include "manager.h"
#include "chaseenemy.h"
#include "manager.h"
#include "fade.h"
#include "bossbar.h"
#include "camera.h"
#include "stage.h"
#include "score.h"
#include "sound.h"
#include "jeteffect.h"
#include "moveeffect.h"
#include "motion.h"
#include "shadow.h"
#include "hp_gauge.h"

CBossbar*CBoss::m_life;
int CBoss::m_Hp;
bool CBoss::m_DesEvent;
CHpGauge *CBoss::m_pHpGauge;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBoss::CBoss()
{
	m_nouevent = 0;
	m_frameCount = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBoss::~CBoss()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBoss::Init()
{
	// 現在のモーション番号の保管
	CEnemy::Init();
	SetUp(CObject::BOSS);
	m_hit = false;
	m_isMove = false;
	m_change = false;
	m_isFly = false;
	m_isTop = false;
	m_DesEvent = false;
	m_MeteoMovesleep = 0;
	m_MeteoMove = 0;
	m_Hp = 10000;
	m_Meteosleep = 0;
	m_Meteo = 0;
	//m_life = CBossbar::Create(D3DXVECTOR3(700.0f, 150.0f, 0.0f), (float)m_Hp);
	m_move = {0.0f,0.0f,0.0f};
	m_Gurabitexi = { 0.0f,0.0f,0.0f };
	m_move.x = -5.0f;
	m_damageEffectCnt = 0;
	m_cooldown = 0;
	m_attack = 0;
	pointCollar = 3.0f;
	m_movelog = 0.0f;
	SetScale({ 0.7f,0.7f ,0.7f });

	m_pHpGauge = CHpGauge::Create(CHpGauge::TYPE_2D);
	m_pHpGauge->SetPos(D3DXVECTOR3(1050.0f, 330.0f, 0.0f));
	m_pHpGauge->SetRot(D3DXVECTOR3(0.0f, 0.0f, -D3DX_PI / 2.0f));
	m_pHpGauge->SetMaxSize(D3DXVECTOR3(50.0f, 400.0f, 0.0f));
	m_pHpGauge->SetRatio(0.9f);
	m_pHpGauge->SetMaxNumber((float)(10000));

	m_active = ACTIVE_NONE;
	m_frameCount = -200;

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBoss::Update()
{
	CEnemy::Update();

	D3DXVECTOR3 Pos = GetPos();
	
	if (Pos.y <= 0.0f)
	{
		m_isFly = false;
	}
	else
	{
		m_isFly = true;
		Pos.y -= 15.0f;
		SetPos(Pos);
	}

	// ダメージを喰らうと通る処理
	DamageEffect();

	if (m_DesEvent)
	{
		m_active = ACTIVE_DEATH;
	}

	switch (m_active)
	{
	case CBoss::ACTIVE_NONE: // 待機中
		GetMotion()->SetNumMotion(NONE);
		Wait();
		break;
	case CBoss::ACTIVE_DEATH: // 死亡時
		Death();
		break;
	case CBoss::ACTIVE_METEO: // 蛇を山なりに投げる攻撃
		GetMotion()->SetNumMotion(METEO);
		Meteo();
		break;
	case CBoss::ACTIVE_MOVE:
		GetMotion()->SetNumMotion(ATTACKMOVE);
		Attack();
		break;
	case CBoss::ACTIVE_ATTACKMOVE:
		GetMotion()->SetNumMotion(MOVE);
		MeteoMove();
		break;
	case CBoss::MAX_ACTIVE:
	default:
		assert(false);
		break;
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBoss *CBoss::Create(D3DXVECTOR3 pos)
{
	CBoss *pEnemy = nullptr;

	pEnemy = new CBoss;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::BOSS);
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CBoss::Death()
{
	D3DXVECTOR3 Pos = GetPos();

	for (int i = 0; i < 3; i++)
	{
		float R = FloatRandom(1.0f, 0.0f);
		float G = FloatRandom(1.0f, 0.0f);
		float B = FloatRandom(1.0f, 0.0f);
		float MoveX = FloatRandom(10.0f, -10.0f);
		float MoveY = FloatRandom(15.0f, 10.0f);
		float MoveZ = FloatRandom(10.0f, -10.0f);

		CMoveEffect*JetEffect = CMoveEffect::Create({ MoveX ,MoveY ,MoveZ });
		JetEffect->SetSize(D3DXVECTOR3(200.0f*i, 200.0f*i, 0.0f));//サイズ設定
		JetEffect->SetColar(D3DXCOLOR(R, G, B, 1.0f));//色設定
		JetEffect->SetPos(Pos);
	}
	Pos.x += m_move.x;
	Pos.y -= 1.0f;
	SetPos(Pos);
	if (Pos.y <= -200.0f)
	{
		CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_RESULT);
	}
	m_change = !m_change;
	if (m_change)
	{
		m_move.x = -1.0f;
		ChangeColar(D3DXCOLOR{ 1.0f,0.0f,0.0f,0.5f });
	}
	else
	{
		m_move.x = 1.0f;
		SetIsColor(false);
	}
}

//--------------------------------------------------
// 蛇を山なりに投げる攻撃
//--------------------------------------------------
void CBoss::Meteo()
{
	CMotion *pMotion = CMotionModel3D::GetMotion();
	m_Meteosleep++;

	if (m_Meteosleep >= METEOTIME)
	{
		CChaseEnemy::Create(GetPos())->SetBossPos(GetPos());
		m_Meteosleep = 0;
		m_Meteo++;
		if (m_Meteo >= METEOMAX)
		{
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_KEITAI);
			m_Meteo = 0;
			m_active = ACTIVE_NONE;
		}
	}
}

//--------------------------------------------------
// ダメージを受けるとしばらくの間エフェクトを行なう
//--------------------------------------------------
void CBoss::DamageEffect()
{
	if (!m_hit || m_DesEvent)
	{
		return;
	}

	m_change = !m_change;
	if (m_change)
	{
		ChangeColar(D3DXCOLOR(1.0f,0.0f,0.0f,0.5f));
	}
	else
	{
		SetIsColor(false);
	}

	m_damageEffectCnt--;
	if (m_damageEffectCnt <= 0)
	{
		SetIsColor(false);
		m_damageEffectCnt = 0;
		m_hit = false;
	}
}

//--------------------------------------------------
// 待機
//--------------------------------------------------
void CBoss::Wait()
{
	m_frameCount++;

	if (m_frameCount >= 150)
	{
		EActiveType next = (EActiveType)IntRandom(MAX_ACTIVE, ACTIVE_NONE + 1);
		m_active = next;
		m_frameCount = 0;
	}

	// 位置の調整
	D3DXVECTOR3 Pos = GetPos();
	CPlayer*Player = CGame::GetStage()->GetPlayer();
	if (Pos.x >= Player->GetPos().x + 1200.0f)
	{
		Pos.x = Player->GetPos().x + 1199.0f;
		m_move.x *= -1.0f;
	}
	if (Pos.x <= -1200.0f)
	{
		Pos.x = -1199.0f;
		m_move.x *= -1.0f;
	}
	Pos += m_move;
	//SetPos(Pos);

	D3DXVECTOR3 pos = GetPos();
	if (pos.z >= CGame::GetStage()->GetPlayer()->GetPos().z + ATTACKBACK / 2)
	{
		Pos.y = Player->GetPos().y;
		//SetPos(pos);
	}
}

//--------------------------------------------------
// meteo動作(突撃)
//--------------------------------------------------
void CBoss::Attack()
{
	D3DXVECTOR3 Pos = GetPos();

	if (m_cooldown <= ATTACKTIME)
	{
		m_posOld = GetPos();			// 過去の移動量を保存
		D3DXVECTOR3 rot = GetRot();

		//modelとプレイヤーの当たり判定
		CPlayer*Player = CGame::GetStage()->GetPlayer();

		D3DXVECTOR3 vecPlayerDist = Player->GetPos() - GetPos();
		float distPlayer = D3DXVec3Length(&vecPlayerDist);
		m_AttackMove = vecPlayerDist / distPlayer * MOVESPEED;
	
		rot.y = atan2f(-vecPlayerDist.x, -vecPlayerDist.z) - D3DX_PI;
		SetRot(rot);
	}
	else if (m_cooldown >= ATTACKBACK)
	{
		Pos.z = CGame::GetStage()->GetPlayer()->GetPos().z + ATTACKBACK / 2;
		Pos.y = 1500;
		SetPos(Pos);
		m_cooldown = 0;
		m_attack++;

		// 発射する時にエフェクトを出す
		for (int i = 0; i < 3; i++)
		{
			CJetEffect*JetEffect = CJetEffect::Create(m_move);
			JetEffect->SetSize(D3DXVECTOR3(200.0f*i, 200.0f*i, 0.0f));	//サイズ設定
			JetEffect->SetColar(D3DXCOLOR(1.0f, 0.3f, 0.3f, 1.0f));		//色設定
			JetEffect->SetPos(Pos);
		}
	}
	else
	{
		CMoveEffect*MoveEffect = CMoveEffect::Create(m_move);
		MoveEffect->SetSize(D3DXVECTOR3(130.0f, 130.0f, 0.0f));		//サイズ設定
 		MoveEffect->SetColar(D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));	//色設定
		MoveEffect->SetPos(D3DXVECTOR3(Pos.x,Pos.y + 30.0f ,Pos.z));
		Pos.x += m_AttackMove.x;
		Pos.z += m_AttackMove.z;
		m_movelog += m_AttackMove.z;
		
		SetPos(Pos);
	}
	
	m_isFly = false;
	m_cooldown++;
	
	if (m_movelog <= -(float)ATTACKBACK)
	{//画面外連れてく
		Pos.z = CGame::GetStage()->GetPlayer()->GetPos().z + ATTACKBACK / 2;
		Pos.y = CGame::GetStage()->GetPlayer()->GetPos().y;
		SetPos(Pos);
		m_movelog = 0.0f;
		m_cooldown = 0;
		m_attack++;
	}
	
	if (m_attack >= (float)ATTACKMAX)
	{
		m_active = ACTIVE_NONE;
		m_movelog = 0.0f;
		m_attack = 0;
		SetRot({ 0.0f,D3DX_PI,0.0f });
	}
}

//--------------------------------------------------
// メテオMove
//--------------------------------------------------
void CBoss::MeteoMove()
{
	D3DXVECTOR3 Pos = GetPos();
	if (m_MeteoMovesleep == 0)
	{
		m_targetPos = CGame::GetStage()->GetPlayer()->GetPos();
		m_Gurabitexi.y = 45.0f;
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_ON);
	}
	m_MeteoMovesleep++;

	if ((m_MeteoMovesleep == METEOMOVETIME))
	{
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_JAMP);
	}
	if (m_MeteoMovesleep <= METEOMOVETIME)
	{
		m_posOld = GetPos();//過去の移動量を保存
		D3DXVECTOR3 rot = GetRot();

		//modelとプレイヤーの当たり判定
		CPlayer* player = CGame::GetStage()->GetPlayer();

		D3DXVECTOR3 vecPlayerDist = player->GetPos() - GetPos();
		float distPlayer = D3DXVec3Length(&vecPlayerDist);
		m_MeteoMoveSpeed = vecPlayerDist / distPlayer * MOVESPEED;

		rot.y = atan2f(-vecPlayerDist.x, -vecPlayerDist.z) - D3DX_PI;
		SetRot(rot);
	}
	else
	{
		if (!m_isTop)
		{
			m_Gurabitexi.y += 6.0f;
			Pos.x += m_MeteoMoveSpeed.x;
			Pos.z += m_MeteoMoveSpeed.z;
			m_movelog += m_MeteoMoveSpeed.z;

		}
		else
		{
			CPlayer* player = CGame::GetStage()->GetPlayer();
			Pos.x = player->GetPos().x;
			Pos.z = player->GetPos().z;
			m_Gurabitexi.y -= 3.0f;
			Pos.y += m_Gurabitexi.y;
		}
		
		Pos.y += m_Gurabitexi.y;
		SetPos(Pos);
	}

	if (Pos.y >= CGame::GetStage()->GetPlayer()->GetPos().y + 1500.0f)
	{//高さ制限
		m_isTop = true;
	}
	
	// 空中にいるとき
	if (Pos.y <= 0.0f)
	{
		if (m_isTop)
		{
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_GURA);
			CRenderer::GetCamera()->ShakeCamera(60, 10.0f);
			if (CGame::GetStage()->GetPlayer()->GetMeshHit())
			{
				CGame::GetStage()->GetPlayer()->SetMoveRock(true);
			}
		}
		Pos.y = 0.0f;
		SetPos(Pos);
		m_isTop = false;
		m_isFly = false;
	}

	if (m_movelog <= -METEOMOVEBACK)
	{//画面外連れてく
		Pos.z = CGame::GetStage()->GetPlayer()->GetPos().z + ATTACKBACK / 2;
		Pos.y = 1500;
		SetPos(Pos);
		m_MeteoMove++;
	}

	if (m_MeteoMove >= METEOMOVEMAX)
	{
		m_movelog = 0.0f;
		m_active = ACTIVE_NONE;
		m_isTop = false;
		m_MeteoMove = 0;
		SetRot({ 0.0f,D3DX_PI,0.0f });

		m_movelog = 0.0f;
		m_MeteoMovesleep = 0;
		SetPos(Pos);
	}

	if (m_MeteoMovesleep == METEOMOVEBACK)
	{
		for (int i = 0; i < 3; i++)
		{
			CJetEffect*JetEffect = CJetEffect::Create(m_move);
			JetEffect->SetSize(D3DXVECTOR3(100.0f * (i + 1), 100.0f * (i + 1), 0.0f));//サイズ設定
			JetEffect->SetColar(D3DXCOLOR(1.0f, 0.0f, 1.0f, 1.0f));//色設定
			JetEffect->SetPos(Pos);
		}
	}
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CBoss::Hit(CObject *object)
{
	switch (object->GetType())
	{
	case CObject::ENEMY:
		SearchModelObject(0, ENEMY, [this](CObject*Enemy)
		{
			CChaseEnemy* pEnemy = dynamic_cast<CChaseEnemy*>(Enemy);
			if (pEnemy != nullptr)
			{
				if (pEnemy->GetThrow())
				{
					CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_BOM);
					CGame::GetStage()->GetScore()->Add(1000);
					CBoss::Add(-10);
					SetcheckHit(true);
					pEnemy->Uninit();
				}
			}
		});
		break;
	case CObject::PLAYER:
	{
		CPlayer*Player = CGame::GetStage()->GetPlayer();
		if (!Player->GetcheckHit())
		{//ダメージ
			if (Player->GetRollMove())
			{
				CGame::GetStage()->GetScore()->Add(5);
				return;
			}
			if (Player->GetAttack())
			{
				return;
			}
			CRenderer::GetCamera()->ShakeCamera(30, 10.0f);
			D3DXVECTOR3 Move = Player->GetMove();
			Player->SetcheckHit(true);
			CGame::GetStage()->GetScore()->Add(-500);
			Player->SetMove({ Move.x,20.0f,Move.z });
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_DAMAGE);
		}
		if (Player->GetAttack())
		{
			if (m_Hp <= 0)
			{
				return;
			}
			CGame::GetStage()->GetScore()->Add(10000);
			CBoss::Add(-1000);
			return;
		}
		break;
	}
	case CObject::BULLET:
	{
		CBullet* pBullet = dynamic_cast<CBullet*>(object);
		if (pBullet == nullptr)
		{
			return;
		}
		CPlayer*Player = CGame::GetStage()->GetPlayer();

		D3DXVECTOR3 Move = Player->GetMove();

		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_BOM);
		CGame::GetStage()->GetScore()->Add(1000);
		CBoss::Add(-pBullet->GetDamage());
		SetcheckHit(true);
		
		object->Uninit();
		//Uninit();
	}
	default:
		break;
	}

}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CBoss::NotHit()
{
}

//--------------------------------------------------
// Hp管理
//--------------------------------------------------
void CBoss::Add(int IsAdd)
{
	CRenderer::GetCamera()->ShakeCamera(30, 10.0f);

	m_Hp += IsAdd;
	//m_life->SetDamage(-IsAdd);
	m_pHpGauge->SetNumber((float)m_Hp);
	if (m_Hp <= 0)
	{
		DesEvent();
	}
}

//--------------------------------------------------
// Hp管理
//--------------------------------------------------
void CBoss::DesEvent()
{
	CRenderer::GetCamera()->ShakeCamera(60, 30.0f);
	m_DesEvent = true;
	CRenderer::GetCamera()->SetEnd(true);
}
