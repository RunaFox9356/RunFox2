//=============================================================================
//
// 説明書
// Author : hamada ryuuga
//
//=============================================================================
#ifndef _STAGE_H_			// このマクロ定義がされてなかったら
#define _STAGE_H_			// 二重インクルード防止のマクロ定義

//-----------------------------------------------------------------------------
// include
//-----------------------------------------------------------------------------
#include "renderer.h"
#include "object.h"
#
//-----------------------------------------------------------------------------
// 前方宣言
//-----------------------------------------------------------------------------
class CMesh;
class CPlayer;
class CBuilding;
class CTimer;
class CScore;
class CShadow;
//-----------------------------------------------------------------------------
// ステージクラス
//-----------------------------------------------------------------------------
class CStage : public CObject
{
public:
	static const int MAX_MODEL = 256;	// テクスチャの最大数

	const std::string REL_PATH = "data/TEXTURE/";
	const std::string ABS_PATH = "data\\TEXTURE\\";


	enum PATTERN
	{//出現パターン
		PATTERN_0 = 0,
		PATTERN_BOSS,
		PATTERN_TITLE,
		PATTERN_TUTORIAL,
		PATTERN_RESULT,
		PATTERN_MAX
	};

	enum MESHTYPE
	{
		MESH = 0,		// 通常
		MESHMOVE,		// 動くゆか
		FLOORINGMOVE,	//滑る床
		RUNMESH,		//乗ったら動く床
		MAXMESH			//あんただれや？
	};

	enum ENEMYTYPE
	{
		ENEMY = 0,		// 通常
		CHASEENEMY,		// Playerを追ってくるやつ
		BOSS,
		MAXENEMY			//あんただれや？
	};

	enum MODELTYPE
	{
		NORMAL = 0,		// 通常
		PENDULUM,		// 振り子
		WOOD,			// 木
		BALL,			// ボール
		RAIL,			// 乗ったら動く床
		CHANGE,			// 乗ったら動く床
		GOAL,			// ゴール
		DASHITEM,			// dashitem
		BANE,			// bame
		BELL,
		CAMERA,			// CAMERA
		MAXMODEL				//あんただれや？
	};

	void SetObject();

	void Loadfile(const char * pFileName);
	void LoadfileEnemy(const char * pFileName);
	void LoadfileMesh(const char * pFileName);

	CStage();
	~CStage() override;
	HRESULT Init()override;
	void Uninit() override;
	void Update() override;
	void Draw()override {} ;

	D3DXVECTOR3 GetPlayerHitMesh(D3DXVECTOR3 Pos, D3DXVECTOR3 PopPos, bool IsShadow =false, CShadow *object = nullptr);
	static CStage* Create(PATTERN Pattern);

	void SetPopItem(char* Name) { m_popItemName = Name; }
	void SetGamePattern(PATTERN& Name) { m_pattern = Name; }

	char* GetPopItem() { return m_popItemName; }
	CPlayer * GetPlayer() { return m_player; }
	CTimer* GetTimer() { return m_timer; }
	CScore* GetScore() { return m_score; }
	int GetNumAllEnemy() { return m_numAllEnemy; };	// 敵の総数の取得
	int GetNumAllBuilding() { return m_numAllBuilding; };	// 総数の取得
	bool GetHitMesh(D3DXVECTOR3 Pos);
private:

	int m_numAllEnemy;
	int m_numAllBuilding;
	char *m_popItemName;
	CPlayer * m_player;
	CTimer* m_timer;
	CScore*m_score;
	PATTERN m_pattern;
};
#endif
