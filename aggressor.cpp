//=============================================================================
//
// 敵設定
// Author:hamada ryuuga
//
//=============================================================================

#include "aggressor.h"
#include "player.h"
#include "game.h"
#include "stage.h"
#include "utility.h"
#include "hamada.h"
#include "line.h"
#include "reaction.h"
#include "boss.h"
#include "score.h"
#include "manager.h"
#include "renderer.h"
#include "sound.h"
#include "renderer.h"
#include "camera.h"
#include "shadow.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CAggressor::CAggressor()
{
	m_Stop = 0;
	m_lookmin = { -500.0f,-500.0f ,-500.0f };
	m_lookmax = { 500.0f,500.0f ,500.0f };
	m_lookCount = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CAggressor::~CAggressor()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CAggressor::Init()
{
	// 現在のモーション番号の保管
	
	CEnemy::Init();
	SetUp(CObject::ENEMY);
	m_hit = false;
	m_isMove = false;
	m_isPlayerHit = false;
	m_aiming = true;
	m_aimingCount = 0;
	m_des = false;
	m_get = false;
	m_sensing = CReaction::Create();
	m_sensing->SetTexture(CTexture::TEXTURE_OFF);
	m_sensing->SetPos(GetPos());
	m_sensing->SetPopEvent(false);
	m_sensing->SetSize(D3DXVECTOR3(50.0f, 50.0f, 0.0f));
	m_sensing->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	m_move = { 0.0f,0.0f,0.0f };
	m_throw = false;
	m_nouHave = 0;
	m_alpha = 0.3f;
	m_isChange = true;
	m_shadow = CShadow::Create(GetPos());
	m_shadow->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.7f));
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CAggressor::Uninit()
{
	CEnemy::Uninit();
	m_shadow->Release();
	m_sensing->Release();
	m_des = true;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CAggressor::Update()
{
	CPlayer*Player = CGame::GetStage()->GetPlayer();
	D3DXVECTOR3  pPos = Player->GetPos();
	D3DXVECTOR3  pPosOld = Player->GetPosOld();
	if (CollisionMove(&pPos, &pPosOld, &D3DXVECTOR3(100.0f, 100.0f, 100.0f)))
	{
		D3DXVECTOR3 pos = GetPos();
		m_sensing->SetPos(D3DXVECTOR3(pos.x, pos.y + 100.0f, pos.z));
		m_sensing->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

		if (m_isChange)
		{
			m_alpha += 0.01f;
		}
		else
		{
			m_alpha -= 0.01f;
		}
		if (m_alpha <= 0.3f)
		{
			m_isChange = true;
		}
		if (m_alpha >= 1.0f)
		{
			m_isChange = false;
		}
		m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_alpha));

		m_Stop++;
		if (m_Stop >= MOVE)
		{
			D3DXVECTOR3 target = pPos;		//必要な位置の設定
			D3DXVECTOR3 rot = GetRot();
			D3DXVECTOR3 vecPlayerDist = target - pos;

			float dist = sqrtf(((target.x - pos.x) * (target.x - pos.x)) + ((target.y - pos.y) * (target.y - pos.y)) + ((target.z - pos.z) * (target.z - pos.z)));
	
			float distPlayer = D3DXVec3Length(&vecPlayerDist);

			m_move += vecPlayerDist / distPlayer * 0.2f;

			rot.y = (atan2f(vecPlayerDist.x, vecPlayerDist.z));
			pos += m_move;

			SetRot(rot);			//目的の回転角度の設定
			SetPos(pos);

		}

	}
	else
	{
		m_sensing->SetPos(GetPos());
		m_sensing->SetColar(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CAggressor::Draw()
{
	CEnemy::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CAggressor *CAggressor::Create(D3DXVECTOR3 pos)
{
	CAggressor *pEnemy = nullptr;

	pEnemy = new CAggressor;

	if (pEnemy != nullptr)
	{
		pEnemy->Init();
		pEnemy->SetPopPos(pos);
		pEnemy->SetEnemyType(CStage::CHASEENEMY);
		pEnemy->SetMotion("data\\system\\enemy\\snake.txt");
	}

	return pEnemy;
}

//--------------------------------------------------
// 動作
//--------------------------------------------------
void CAggressor::Move()
{
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
void CAggressor::Hit(CObject *object)
{
	switch (object->GetType())
	{
	case CObject::PLAYER:
	{
		CPlayer*Player = CGame::GetStage()->GetPlayer();
		if (!Player->GetcheckHit())
		{//ダメージ
			if (Player->GetRollMove())
			{
				CGame::GetStage()->GetScore()->Add(5);
				return;
			}
			CRenderer::GetCamera()->ShakeCamera(30, 10.0f);
			D3DXVECTOR3 Move = Player->GetMove();
			Player->SetcheckHit(true);
			CGame::GetStage()->GetScore()->Add(-500);
			Player->SetMove({ Move.x,20.0f,Move.z });
			CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_DAMAGE);
		}
		if (Player->GetAttack())
		{
		
			CGame::GetStage()->GetScore()->Add(10000);
			CBoss::Add(-1000);
			Uninit();
			return;
		}
		break;
	}
	case CObject::BULLET:
	{
		CBullet* pBullet = dynamic_cast<CBullet*>(object);
		if (pBullet == nullptr)
		{
			return;
		}
		CPlayer*Player = CGame::GetStage()->GetPlayer();

		D3DXVECTOR3 Move = Player->GetMove();

		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_SE_BOM);
		CGame::GetStage()->GetScore()->Add(100);
		CBoss::Add(-pBullet->GetDamage());
		object->Uninit();
		Uninit();
		//Uninit();
	}
	default:
		break;
	}
}
//--------------------------------------------------
// 当たってないときの判定
//--------------------------------------------------
void CAggressor::NotHit()
{
	
}

//--------------------------------------------------
// こりじょん判定
//--------------------------------------------------
bool CAggressor::CollisionMove(D3DXVECTOR3 * pPos, D3DXVECTOR3 * pPosOld, D3DXVECTOR3 * pSize)
{

	bool bIsLanding = false;

	D3DXMATRIX mtxWorld = *CMotionModel3D::GetMtxWorld();

	D3DXVECTOR3 min = GetlookMin();
	D3DXVECTOR3 max = GetlookMax();

	// 座標を入れる箱
	D3DXVECTOR3 localPos[4];
	D3DXVECTOR3 worldPos[4];

	// ローカルの座標
	localPos[0] = D3DXVECTOR3(min.x, 0.0f, max.z);
	localPos[1] = D3DXVECTOR3(max.x, 0.0f, max.z);
	localPos[2] = D3DXVECTOR3(max.x, 0.0f, min.z);
	localPos[3] = D3DXVECTOR3(min.x, 0.0f, min.z);

	for (int nCnt = 0; nCnt < 4; nCnt++)
	{// ローカルからワールドに変換
		D3DXVECTOR3 vec = localPos[nCnt];
		D3DXVec3Normalize(&vec, &vec);
		// 大きめにとる
		localPos[nCnt] += (vec * 50.0f);

		D3DXVec3TransformCoord(&worldPos[nCnt], &localPos[nCnt], &mtxWorld);

	}

	D3DXVECTOR3 vecPlayer[4];

	// 頂点座標の取得
	vecPlayer[0] = *pPos - worldPos[0];
	vecPlayer[1] = *pPos - worldPos[1];
	vecPlayer[2] = *pPos - worldPos[2];
	vecPlayer[3] = *pPos - worldPos[3];

	D3DXVECTOR3 vecLine[4];

	// 四辺の取得 (v2)
	vecLine[0] = worldPos[1] - worldPos[0];
	vecLine[1] = worldPos[2] - worldPos[1];
	vecLine[2] = worldPos[3] - worldPos[2];
	vecLine[3] = worldPos[0] - worldPos[3];

	float InOut[4];

	InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
	InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
	InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);
	InOut[3] = Vec2Cross(&vecLine[3], &vecPlayer[3]);

	D3DXVECTOR3 pos = GetPos();

	if (InOut[0] < 0.0f && InOut[1] < 0.0f && InOut[2] < 0.0f && InOut[3] < 0.0f)
	{
		//if (pPos->y < pos.y + max.y && pPos->y + pSize->y > pos.y)
		{
			bIsLanding = true;
		}
	}
	else
	{

	}

	return bIsLanding;
}



