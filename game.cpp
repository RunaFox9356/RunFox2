//============================
//
// ゲーム画面
// Author:hamada ryuuga
//
//============================

//-----------------------------------------------------------------------------
// インクルード
//-----------------------------------------------------------------------------
#include "game.h"
#include "input.h"
#include "manager.h"
#include "light.h"
#include "objectX.h"
#include "fade.h"
#include "multiply.h"
#include "particle_manager.h"
#include "score.h"
#include "sound.h"
#include "pause.h"
#include "stage.h"
#include "camera.h"
#include "aggressor.h"
#include "magic.h"
//-----------------------------------------------------------------------------
// 静的メンバー変数の宣言
//-----------------------------------------------------------------------------
CPause *CGame::m_Pause = nullptr;
CStage* CGame::m_Stage = nullptr;
int CGame::m_GameScore = 0;
CParticleManager*CGame::m_PaticleManager = nullptr;
CMagicBox* CGame::m_MagicBox;
//=============================================================================
// コンストラクター
//=============================================================================
CGame::CGame()
{
	m_pattern = CStage::PATTERN_0;
}

//=============================================================================
// デストラクト
//=============================================================================
CGame::~CGame()
{
}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CGame::Init(void)
{

	m_PaticleManager = new CParticleManager;
	// パーティクル
	if (FAILED(m_PaticleManager->Init()))
	{
		return E_FAIL;
	}

	m_GameCount = 0;
	m_SpeedUp = 300;
	m_nCntSpawn = 0;
	
//	CManager::GetInstance()->GetRenderer()->SetFog(true, D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	CManager::GetInstance()->GetRenderer()->GetCamera()->GameInit();
	m_GameScore = 0;

	m_MagicBox = CMagicBox::Create(D3DXVECTOR3(150.0f, 620.0f, 0.0f));

	m_MagicBox->CMagicBox::Magicplay(CTexture::TEXTURE_THUNDER);
	m_MagicBox->CMagicBox::Magicplay(CTexture::TEXTURE_ICE);
	m_MagicBox->CMagicBox::Magicplay(CTexture::TEXTURE_FIRE);

	m_Pause = new CPause;
	m_Pause->Init();
	m_Pause->SetUp(CObject::PAUSE);

	CAggressor::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_Stage = CStage::Create(m_pattern);

	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CGame::Uninit(void)
{
	if (m_PaticleManager != nullptr)
	{
		m_PaticleManager->Uninit();
		delete m_PaticleManager;
		m_PaticleManager = nullptr;

	}
	if (m_Pause != nullptr)
	{
		m_Pause->Uninit();
		m_Pause = nullptr;
	}

	CObject::Release();
	//CObjectXManager::ReleaseAll();
}

//=============================================================================
// 更新
//=============================================================================
void CGame::Update(void)
{
	//CInput *CInputpInput = CInput::GetKey();
	//
	//if (CInputpInput->Trigger(CInput::KEY_DEBUG))
	//{
	//	//モードの設定
	//	CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_BOSS);
	//	return;
	//}
	//if (CInputpInput->Trigger(CInput::KEY_DELETE))
	//{
	//	CManager::GetInstance()->GetFade()->NextMode(CManager::MODE_RESULT);
	//	return;
	//}
	m_PaticleManager->Update();
}

//=============================================================================
// 描画
//=============================================================================
void CGame::Draw(void)
{
}
