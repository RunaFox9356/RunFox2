//============================
//
// state設定
// Author:hamada ryuuga
//
//============================

#include "stage.h"
#include "object.h"
#include "utility.h"

#include "letter.h"
#include "manager.h"

#include "building.h"
#include "mesh.h"
#include "movemesh.h"
#include "meshroom.h"
#include "runmesh.h"
#include "player.h"

#include "input.h"
#include "objectX.h"

#include "ball.h"
#include "wood_spawn.h"
#include "pendulum.h"

#include "stage.h"
#include "game.h"

#include "manager.h"
#include "renderer.h"
#include "rail.h"
#include "timer.h"
#include "score.h"

#include "dashitem.h"
#include "sprint.h"
#include "skyfield.h"
#include "3dpolygon.h"
#include "land.h"
#include "bell.h"
#include "camera.h"
#include "camera_player.h"

#include "enemy.h"
#include "chaseenemy.h"
#include "boss.h"

#include "shadow.h"
#include "sound.h"

namespace nl = nlohmann;

nl::json JBuilding;//リストの生成
nl::json JlistMesh;//リストの生成
nl::json JlistEnemy;//リストの生成

//------------------------------------
// コンストラクタ
//------------------------------------
CStage::CStage()
{
	m_pattern = CStage::PATTERN_0;
	m_numAllBuilding = 0;
	m_numAllEnemy = 0;
}

//------------------------------------
// デストラクタ
//------------------------------------
CStage::~CStage()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT  CStage::Init()
{
	CRenderer::GetCamera()->SetEnd(false);
	if (*CManager::GetInstance()->GetMode() == CManager::MODE_GAME || *CManager::GetInstance()->GetMode() == CManager::MODE_BOSS)
	{//ゲームに必要なもの
		m_player = CPlayer::Create();
		m_player->SetMotion("data/SYSTEM/Gon/Fox.txt");
		m_player->SetPos({ 400.0f ,0.0f,-700.0f });
		
		m_numAllBuilding = 0;
	}
	switch (m_pattern)
	{
	case PATTERN_0:
		//ステージのゆか情報読み込み
		CRenderer::GetCamera()->LandAnimation("CameraAnimation");
		//CStage::LoadfileMesh("data\\MESH\\Game001.json");
		//CStage::Loadfile("data\\MODEL.json");
		//CStage::LoadfileEnemy("data\\ENEMY.json");
		CStage::LoadfileEnemy("data\\bossEnemy.json");
		m_player->SetReSpoonPos({ 400.0f ,0.0f,-300.0f });	
		m_player->SetPos({ 400.0f ,0.0f,-700.0f });
		m_player->GetShadow()->SetPos({ 400.0f ,3.0f,-300.0f });
		CRenderer::GetCamera()->PlayAnimation("CameraAnimation", false, [this](void)
		{
			m_score = CScore::Create(D3DXVECTOR3(500.0f, 30.0f, 0.0f));
			m_timer = CTimer::Create({ 1140.0f,100.0f,0.0f }, { 50.0f,50.0f,0.0f }, 180);
			m_score->Set(1000);
			m_player->SetIsMove(true);
			
		});
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_BGM_GAME);
	
		break;
	case PATTERN_BOSS:
		m_score = CScore::Create(D3DXVECTOR3(500.0f, 30.0f, 0.0f));
		m_timer = CTimer::Create({ 1140.0f,100.0f,0.0f }, { 50.0f,50.0f,0.0f }, 600);
		m_player->SetIsMove(true);
		CStage::Loadfile("data\\boss.json");
		CStage::LoadfileMesh("data\\MESH\\boss.json");
		CStage::LoadfileEnemy("data\\bossEnemy.json");
		m_player->SetPos({ 400.0f ,0.0f,-700.0f });
		m_player->SetReSpoonPos({ 400.0f ,0.0f,-700.0f });
		m_player->GetShadow()->SetPos(m_player->GetPos());
		CManager::GetInstance()->GetSound()->Play(CSound::LABEL_BGM_BOSS2);
		break;
	case PATTERN_TITLE:
		CStage::LoadfileMesh("data\\MESH\\test.json");
		CStage::Loadfile("data\\testBil.json");
		CStage::LoadfileEnemy("data\\TITLEEnemy.json");
		break;
	
	case PATTERN_TUTORIAL:
		CStage::LoadfileMesh("data\\MESH\\test.json");
		CStage::Loadfile("data\\testBil.json");
		CStage::LoadfileEnemy("data\\TITLEEnemy.json");

		break; 
	case PATTERN_RESULT:
		CStage::LoadfileMesh("data\\MESH\\GAMECLEARME.json");
		CStage::Loadfile("data\\GAMECLEARMODEL.json");
		CStage::LoadfileEnemy("data\\GAMECLEAR.json");
		break;

	default:
		break;
	}


	//ゆかのポリゴン読み込み
	CLand* Data = CLand::Create();
	Data->SetSize({ 1000000.0f, 0.0f, 1000000.0f });
	Data->SetPos({ 0.0f,-1000.0f,0.0f });
	Data->SetTexture(CTexture::TEXTURE_GIN);

	
	//スカイメッシュ読み込み
	CSkyField::Create();

	
	return S_OK;
}


//------------------------------------
//オブジェクトの生成
//------------------------------------
void CStage::SetObject()
{

	
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::Loadfile(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JBuilding;
		nIndex = JBuilding["INDEX"];
		m_numAllBuilding = JBuilding["INDEX"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 size;
		D3DXVECTOR3 rot;
		std::string Type;
		bool gold;
		bool Check;
		int ModelType = 0;
		
		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			CBuilding* Buil = nullptr;
			std::string name = "BUILDING";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			if (JBuilding.count(name) == 0)
			{
				
			}
			pos = D3DXVECTOR3(JBuilding[name]["POS"]["X"], JBuilding[name]["POS"]["Y"], JBuilding[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JBuilding[name]["ROT"]["X"], JBuilding[name]["ROT"]["Y"], JBuilding[name]["ROT"]["Z"]);
			Type = JBuilding[name]["TYPE"];
			ModelType = JBuilding[name]["MODELTYPE"];
			gold = JBuilding[name]["GOLD"];
			Check = JBuilding[name]["CHECK"];
			switch ((CStage::MODELTYPE)ModelType)
			{
			case CStage::NORMAL:
			{
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			}
			case CStage::BALL:
			{
				float radius;
				float Rotspeed;
				int Rottype;
				D3DXVECTOR3 centerpos;

				name += "BALL";
				centerpos = D3DXVECTOR3(JBuilding[name]["CENTERPOS"]["X"], JBuilding[name]["CENTERPOS"]["Y"], JBuilding[name]["CENTERPOS"]["Z"]);
				radius = JBuilding[name]["RADIUS"];
				Rotspeed = JBuilding[name]["ROTSPEED"];
				Rottype = JBuilding[name]["ROTTYPE"];

				Buil = CBall::Create(centerpos, radius, Rotspeed, Rottype);

				break;
			}
			case CStage::PENDULUM:
			{
				D3DXVECTOR3 Ballrot;
				float Coefficient;
				int Movetype;

				std::string BallType;


				name += "PENDULUM";

				Ballrot = D3DXVECTOR3(JBuilding[name]["DESTROT"]["X"], JBuilding[name]["DESTROT"]["Y"], JBuilding[name]["DESTROT"]["Z"]);
				Coefficient = JBuilding[name]["COEFFICIENT"];
				Movetype = JBuilding[name]["MOVETYPE"];
				Buil = CPendulum::Create(pos, Ballrot, Coefficient, Movetype);

				break;
			}
			case CStage::WOOD:
			{
				D3DXVECTOR3 Woodpos;
				float Poprot;
				int Poptime;
				float PoprotSpeed;
				float PopMove;

				std::string WoodType;

				name += "WOOD";

				Woodpos = D3DXVECTOR3(JBuilding[name]["POPPOS"]["X"], JBuilding[name]["POPPOS"]["Y"], JBuilding[name]["POPPOS"]["Z"]);
				Poprot = JBuilding[name]["POPROT"];
				Poptime = JBuilding[name]["POPTIME"];
				PoprotSpeed = JBuilding[name]["ROTSPEED"];
				PopMove = JBuilding[name]["MOVE"];

				Buil = CWood_Spawn::Create(Woodpos, Poprot, PopMove, PoprotSpeed, Poptime);
				break;
			}
			case CStage::RAIL:
				Buil = CRail::Create(pos, rot);
				break;
			case CStage::CHANGE:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetChangePoptime(Check);
				break;
			case CStage::GOAL:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			case CStage::DASHITEM:
				Buil = CDashitem::Create(pos, rot);
				break;
			case CStage::BANE:
				Buil = CSprint::Create(pos, rot);
				break;
			case CStage::BELL:
				Buil = CBell::Create(pos, rot);
				break;
			case CStage::CAMERA:
			{
				name += "CAMERA";
				std::string PopPass = JBuilding[name]["PLAYERPASS"];
				Buil = CCamera_Player::Create(pos, rot, PopPass);

			}
			default:
				break;
			}

			Buil->SetUp(BUILDING);
			Buil->SetModelType((CStage::MODELTYPE)ModelType);
		
		}
	}
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::LoadfileMesh(const char * pFileName)
{
	std::ifstream ifs(pFileName);
	
	if (ifs)
	{
		ifs >> JlistMesh;

		int nIndex = 0;

		nIndex = JlistMesh["INDEX"];

		int Type;

		for (int meshCount = 0; meshCount < nIndex; meshCount++)
		{
			CMesh*mesh = nullptr;
			std::string name = "Mesh";
			std::string Number = std::to_string(meshCount);
			name += Number;
			std::string filepass;

			filepass = JlistMesh[name]["FILEPASS"];
			Type = JlistMesh[name]["TYPE"];


			switch ((MESHTYPE)Type)
			{
			case MESH:
				mesh = CMesh::Create();
				break;
			case MESHMOVE:
				mesh = CMoveMesh::Create();
				break;
			case FLOORINGMOVE:
				mesh = CMeshRoom::Create();
				break;
			case RUNMESH:
				mesh = CRunMesh::Create();
				break;
			default:
				mesh = CMesh::Create();
				break;
			}

			mesh->Loadfile(filepass.c_str());
			mesh->SetType(Type);
			mesh->SetNumber(meshCount);
		}
	}
}


//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::LoadfileEnemy(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JlistEnemy;
		nIndex = JlistEnemy["INDEX"];
		m_numAllEnemy = JlistEnemy["INDEX"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 Min;
		D3DXVECTOR3 Max;
		D3DXVECTOR3 rot;
		std::string Type;
		int ModelType = 0;

		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			CEnemy* Enemy = nullptr;
			std::string name = "ENEMY";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			pos = D3DXVECTOR3(JlistEnemy[name]["POS"]["X"], JlistEnemy[name]["POS"]["Y"], JlistEnemy[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JlistEnemy[name]["ROT"]["X"], JlistEnemy[name]["ROT"]["Y"], JlistEnemy[name]["ROT"]["Z"]);
			Min = D3DXVECTOR3(JlistEnemy[name]["MIN"]["X"], JlistEnemy[name]["MIN"]["Y"], JlistEnemy[name]["MIN"]["Z"]);
			Max = D3DXVECTOR3(JlistEnemy[name]["MAX"]["X"], JlistEnemy[name]["MAX"]["Y"], JlistEnemy[name]["MAX"]["Z"]);

			Type = JlistEnemy[name]["PASS"];
			ModelType = JlistEnemy[name]["TYPE"];

			switch ((CStage::MODELTYPE)ModelType)
			{
			case CStage::ENEMY:
				Enemy = CEnemy::Create(pos);
				break;
			case CStage::CHASEENEMY:
				Enemy = CChaseEnemy::Create(pos);
				break;
			case CStage::BOSS:
			{
				Enemy = CBoss::Create(pos);
				break;
			}
			default:
				Enemy = CEnemy::Create(pos);
				break;
			}
			Enemy->SetMin(Min);
			Enemy->SetMax(Max);
			Enemy->SetRot(rot);
			Enemy->SetPass(Type);
			if ((CStage::MODELTYPE)ModelType!= CStage::BOSS)
			{
				Enemy->SetUp(CObject::ENEMY);
			}
		
		}
	}
}
//------------------------------------
// 終了
//------------------------------------
void CStage::Uninit()
{
	CManager::GetInstance()->GetSound()->Stop();
	
	
	CObject::Release();
}

//------------------------------------
// 更新
//------------------------------------
void CStage::Update()
{

	
}

//------------------------------------
// create
//------------------------------------
CStage *CStage::Create(CStage::PATTERN Pattern)
{
	CStage * pObject = new CStage;

	if (pObject != nullptr)
	{
		pObject->SetGamePattern(Pattern);
		pObject->Init();
	}
	return pObject;
}



//------------------------------------
// 当たってるメッシュをとる関数
//------------------------------------
D3DXVECTOR3 CStage::GetPlayerHitMesh(D3DXVECTOR3 Pos, D3DXVECTOR3 PopPos,bool IsShadow, CShadow *object)
{
	D3DXVECTOR3 *StartPos = &Pos;

	SearchModelObject(PRIORITY_OBJECT, CObject::MESH, [this, StartPos, IsShadow, object, PopPos](CObject*Mesh)
	{

		CMesh* CastMesh = dynamic_cast<CMesh*>(Mesh);  // ダウンキャスト
		if (CastMesh != nullptr)
		{
			D3DXVECTOR3 Size(CastMesh->GetMeshSize());
			D3DXVECTOR3 oneboxSize(CastMesh->GetOneMeshSize());
			if (((CastMesh->GetPos()->z - Size.z) <= (StartPos->z)) &&
				((CastMesh->GetPos()->z) + oneboxSize.z >= (StartPos->z)) &&
				((CastMesh->GetPos()->x - oneboxSize.x) <= (StartPos->x)) &&
				((CastMesh->GetPos()->x + Size.x) >= StartPos->x))
			{
				if (!CastMesh->GetDeath())
				{
					if (IsShadow)
					{
						D3DXVECTOR3 ShadowPos = PopPos;
						if (CastMesh->CollisionMeshModel(StartPos, &ShadowPos))
						{
							object->SetColar({ 1.0f,1.0f, 1.0f, 0.7f });
						}
					}
					else
					{
						CastMesh->CollisionMesh(StartPos);
					}
					
				}
			}
		}
	});

	return *StartPos;
}

//------------------------------------
// 当たってるメッシュをとる関数
//------------------------------------
bool CStage::GetHitMesh(D3DXVECTOR3 Pos)
{
	D3DXVECTOR3 *StartPos = &Pos;

	bool Hit = false;
	SearchModelObject(PRIORITY_OBJECT, CObject::MESH, [this, StartPos, &Hit](CObject*Mesh)
	{

		CMesh* CastMesh = dynamic_cast<CMesh*>(Mesh);  // ダウンキャスト
		if (CastMesh != nullptr)
		{
			D3DXVECTOR3 Size(CastMesh->GetMeshSize());
			D3DXVECTOR3 oneboxSize(CastMesh->GetOneMeshSize());
			if (((CastMesh->GetPos()->z - Size.z) <= (StartPos->z)) &&
				((CastMesh->GetPos()->z) + oneboxSize.z >= (StartPos->z)) &&
				((CastMesh->GetPos()->x - oneboxSize.x) <= (StartPos->x)) &&
				((CastMesh->GetPos()->x + Size.x) >= StartPos->x))
			{
				Hit = true;
			}
		}
	});

	return Hit;
}




