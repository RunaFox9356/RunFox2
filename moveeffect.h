//=============================================================================
//
// effectオブジェクト
// Author : hamada ryuuga
//
//=============================================================================


#ifndef _MOVEEFFECT_H_			// このマクロ定義がされてなかったら
#define _MOVEEFFECT_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "3dpolygon.h"
#include "texture.h"

class CMoveEffect : public C3dpolygon
{
public:

	static CMoveEffect *CMoveEffect::Create(D3DXVECTOR3 Move);

	CMoveEffect(const int list);
	~CMoveEffect() override;
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	const D3DXVECTOR3 *GetPos() const override;
	void SetPos(const D3DXVECTOR3 &pos) override;
	void SetLife(const int &Life) { m_life = Life; };

private:
	void move();
	int m_life;
};

#endif

